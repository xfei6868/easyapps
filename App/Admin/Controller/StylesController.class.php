<?php
namespace Admin\Controller;
use Think\Controller;
class StylesController extends CommonController {
    public function index()
    {
        $Styles = M('Styles');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['tpl'] = $this->appinfo['tpl'];
        $styles = $Styles->where($map)->select();
        $vo = array();
        foreach ($styles as $k) {
            $vo[$k['cls']] = $k['content'];
        }
        $this->assign('vo',$vo);
        $this->display();
    }

    public function save()
    {
        foreach ($_POST as $k => $v) {
            $this->updateField($k,$v);
        }
    }


   	public function updateField($cls = '',$fvalue='')
    {
        if (!$cls) return;

        $Styles = M('Styles');
        $map['mid'] = $this->mid;
        $map['cls'] = $cls;
        $map['appid'] = $this->appid;
        $map['tpl'] = $this->appinfo['tpl'];
        $data = $map;
        $data['content'] = $fvalue;
        $va = $Styles->where($map)->find();
        if (!$va)  
            $Styles->add($data);
        else
            $Styles->where($map)->save($data);

    }
}