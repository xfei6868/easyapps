<?php
namespace Admin\Controller;
use Think\Controller;
class WidgetController extends CommonController {
    public function index()
    {
        $tmp_dir = "Addon/Widget/";
        $volist = array();
        $list = scandir($tmp_dir);
        foreach($list as $tpl_name){
            $tpl_dir = $tmp_dir."/".$tpl_name;
            if (!is_dir($tpl_dir)) continue;
            $manifest = $tpl_dir = $tmp_dir."/".$tpl_name.'/manifest.xml';
            if (!file_exists($manifest)) continue;
            $preview = $tpl_dir = $tmp_dir."/".$tpl_name.'/preview.jpg';
            if (!file_exists($preview)) continue;
            $v = json_decode(json_encode((array) simplexml_load_file($manifest,'SimpleXMLElement', LIBXML_NOCDATA )), true);
            $vo['name'] = $tpl_name;
            $vo['title'] = $v['title'];
            $vo['author'] = $v['author'];
            $vo['versionCode'] = $v['versionCode'];
            $vo['email'] = $v['email'];
            $vo['url'] = $v['url'];
            $vo['desc'] = $v['desc'];
            $vo['install'] = $v['install'];
            $vo['preview'] = $preview;
            $vo['model'] = 'Widget';
            $volist[] = $vo;
        }
        $widget = json_encode($volist);
        $this->assign('widget',$widget);
        $this->display();
    }

    public function uninstall()
    {
        $model = I('model');
        $name = I('name');
        $install_dir = "Addon/$model/".$name;
        if (!is_dir($install_dir)) {
            $data['ret'] = '0';
            $data['msg'] = '该插件不存在';
            $this->ajaxReturn($data);
            return;
        }
        $manifest = $install_dir.'/manifest.xml';
        $str = file_get_contents($manifest);
        $str = str_replace("<install>true</install>", "<install>false</install>", $str);
        $len = file_put_contents($manifest, $str);
        if ($len == 0) {
            $data['ret'] = '0';
            $data['msg'] = '卸载失败，未知错误';
            $this->ajaxReturn($data);
            return;
        }
        $this->rmWidget($name);
        $data['ret'] = '1';
        $data['msg'] = '卸载完成';
        $this->ajaxReturn($data);
        return;
    }

    public function install()
    {
        $model = I('model');
        $name = I('name');
        $install_dir = "Addon/$model/".$name;
        if (!is_dir($install_dir)) {
            $data['ret'] = '0';
            $data['msg'] = '该插件不存在';
            $this->ajaxReturn($data);
            return;
        }
        $manifest = $install_dir.'/manifest.xml';
        $str = file_get_contents($manifest);
        $str = str_replace("<install>false</install>", "<install>true</install>", $str);
        $len = file_put_contents($manifest, $str);
        if ($len == 0) {
            $data['ret'] = '0';
            $data['msg'] = '安装失败，未知错误';
            $this->ajaxReturn($data);
            return;
        }
        $this->copyWidget($install_dir,$name);
        $data['ret'] = '1';
        $data['msg'] = '安装完成';
        $this->ajaxReturn($data);
        return;
    }

    //复制挂件所需要的文件
    public function copyWidget($addonDir = '',$addonName = ''){
        //Admin 模块
        $dir = $addonDir.'/Admin';
        if (is_dir($dir)) {
            # code...
        }
        //Mobile 模块
        $dir = $addonDir.'/Mobile';
        if (is_dir($dir)) {
            $view_dir = $dir.'/View';
            if (is_dir($view_dir)) {
                $list = scandir($view_dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    $f = $view_dir."/".$tname;
                    $to_dir = "App/Mobile/View/Widget/".$addonName;
                    if (!is_dir($to_dir)) {
                        $d = mkdir($to_dir,0777,true);
                    }
                    $to_file = $to_dir.'/'.$tname;
                    copy($f,$to_file);
                }
            }
            $controller_Dir = $dir.'/Controller';
            if (is_dir($controller_Dir)) {
                $list = scandir($controller_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    $f = $controller_Dir."/".$tname;
                    $to_dir = "App/Mobile/Controller/".$addonName;
                    if (!is_dir($to_dir)) {
                        $d = mkdir($to_dir,0777,true);
                    }
                    $to_file = $to_dir.'/'.$tname;
                    copy($f,$to_file);
                }
            }
            $model_Dir = $dir.'/Model';
            if (is_dir($controller_Dir)) {
                $list = scandir($model_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    $f = $model_Dir."/".$tname;
                    $to_dir = "App/Mobile/Model/".$addonName;
                    if (!is_dir($to_dir)) {
                        $d = mkdir($to_dir,0777,true);
                    }
                    $to_file = $to_dir.'/'.$tname;
                    copy($f,$to_file);
                }
            }
        }
        //Rest 模块
        $dir = $addonDir.'/Rest';
        if (is_dir($dir)) {
            $controller_Dir = $dir.'/Controller';
            if (is_dir($controller_Dir)) {
                $list = scandir($controller_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    $f = $controller_Dir."/".$tname;
                    $to_dir = "App/Rest/Controller/".$addonName;
                    if (!is_dir($to_dir)) {
                        $d = mkdir($to_dir,0777,true);
                    }
                    $to_file = $to_dir.'/'.$tname;
                    copy($f,$to_file);
                }
            }
            $model_Dir = $dir.'/Model';
            if (is_dir($model_Dir)) {
                $list = scandir($model_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    $f = $model_Dir."/".$tname;
                    $to_dir = "App/Rest/Model/".$addonName;
                    if (!is_dir($to_dir)) {
                        $d = mkdir($to_dir,0777,true);
                    }
                    $to_file = $to_dir.'/'.$tname;
                    copy($f,$to_file);
                }
            }
        }
    }

    //删除挂件
    function rmWidget($addonName = ''){
        //Admin 模块
        $dir = '/Admin';
        if (is_dir($dir)) {
            # code...
        }
        //Mobile 模块
        $dir = 'App/Mobile/';
        if (is_dir($dir)) {
            $view_dir = $dir.'View/Widget/'.$addonName;
            if (is_dir($view_dir)) {
                $list = scandir($view_dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    unlink($view_dir.'/'.$tname);
                }
                rmdir($view_dir);
            }
            $controller_Dir = $dir.'/Controller';
            if (is_dir($controller_Dir)) {
                $list = scandir($controller_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    unlink($controller_Dir.'/'.$tname);
                }
                rmdir($controller_Dir);
            }
            $model_Dir = $dir.'Model/Widget/'.$addonName;
            if (is_dir($model_Dir)) {
                $list = scandir($model_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    unlink($model_Dir.'/'.$tname);
                }
                rmdir($model_Dir);
            }
        }
        //Rest 模块
        $dir = 'App/Rest/';
        if (is_dir($dir)) {
            $controller_Dir = $dir.'/Controller';
            if (is_dir($controller_Dir)) {
                $list = scandir($view_dir);
                foreach($list as $tname){
                    unlink($controller_Dir.'/'.$tname);
                }
                rmdir($controller_Dir);
            }
            $model_Dir = $dir.'/Model';
            if (is_dir($model_Dir)) {
                $list = scandir($model_Dir);
                foreach($list as $tname){
                    if ($tname == '..' || $tname == '.') continue;
                    unlink($model_Dir.'/'.$tname);
                }
                rmdir($model_Dir);
            }
        }
    }
}