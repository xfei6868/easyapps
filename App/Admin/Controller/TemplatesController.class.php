<?php
namespace Admin\Controller;
use Think\Controller;
class TemplatesController extends CommonController {
    public function index()
    {
        $tmp_dir = "App/Mobile/View/";
        $volist = array();
        $list = scandir($tmp_dir);
        foreach($list as $tpl_name){
            $tpl_dir = $tmp_dir."/".$tpl_name;
            if (!is_dir($tpl_dir)) continue;
            $manifest = $tpl_dir = $tmp_dir."/".$tpl_name.'/manifest.xml';
            if (!file_exists($manifest)) continue;
            $preview = $tpl_dir = $tmp_dir."/".$tpl_name.'/preview.jpg';
            if (!file_exists($preview)) continue;
            $vo = json_decode(json_encode((array) simplexml_load_file($manifest,'SimpleXMLElement', LIBXML_NOCDATA )), true);
            $vo['preview'] = $preview;
            $vo['tpl_name'] = $tpl_name;
            $volist[] = $vo;
        }
        $this->assign('temp',json_encode($volist));
        $this->display();
    }

    public function useTheme()
    {
        $tpl = I('theme');
        if (!$tpl) return;
        $AppInfo = M('AppInfo');
        $map['mid'] = $this->mid;
        $map['id'] = $this->appid;
        $AppInfo->where($map)->setField('tpl',$tpl);
        $this->appinfo['tpl'] = $tpl;
        $this->assign('appinfo',$this->appinfo);
    }
}