<?php
namespace Admin\Controller;
use Think\Controller;
class NavController extends CommonController {
    public function index()
    {
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map,'idx asc,id desc');
        $this->assign('navs',$navs);
        $this->display();
    }

    public function updateField($name= '',$pk = 0,$value='')
    {
        if (!$pk || !$name) return;
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['id'] = $pk;
        if ($value == 'index') $value = 1;
        $data[$name] = $value;

        $Nav->where($map)->save($data);
        echo( $value );
        # code...
    }

    public function design()
    {
    	$Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map);
        // print_r($navs);
        // exit();
        $this->assign('navs',json_encode($navs));
        // $this->assign('navs',$navs);
        $this->display();
    }

    public function insert()
    {
        $_POST['mid'] = $this->mid;
        $_POST['appid'] = $this->appid;
        $module = I('module');

        $Nav = D('Nav');
        if ( false === $Nav->create() ) {
          $this->error( $Nav->getError() );
        }
        //保存当前数据对象
        $navId = $Nav->add();
        if ( $navId !== false ) { //保存成功
            redirect(U('Admin/Nav/index'));
            // $this->success( '新增成功!', cookie( '_currentUrl_' ) );
        } else {
          //失败提示
          $this->error( '新增失败!' );
        }
    }

    public function delete($id = 0)
    {
        if ($id == 0) return;
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['id'] = $id;
        $Nav->where($map)->delete();
    }

    public function edit($id = 0)
    {
        $Nav = D('Nav');
        $map['id'] = $id;
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $vo = $Nav->where($map)->find();
        $this->assign('vo',$vo);
        $this->display();
    }

    function update($id = 0) 
    {
        $Nav = D('Nav');
        if ( false === $Nav->create() ) {
          $this->error( $Nav->getError() );
        }
        // 更新数据
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['id'] = $id;
        $navId = $Nav->where( $map )->save();
        if ( false !== $navId ) {
          //成功提示
            redirect(U('Admin/Nav/index'));
        } else {
          //错误提示
          $this->error( '编辑失败!' );
        }
    }

    public function add()
    {
    	$this->display();
    }
}